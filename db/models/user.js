const mongoose = require('mongoose');
const userSchema = new mongoose.Schema({
  fullName: {
    type: String
  },
  mobileNumber: {
    type: String,
    required: true
  },
  otp: String
});
const userModel = mongoose.model('Users',userSchema);
module.exports = userModel;