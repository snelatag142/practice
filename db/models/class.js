const mongoose = require('mongoose');

const classSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    unique: true
  }
});

const classModel = mongoose.model('Class', classSchema);

module.exports = classModel;
