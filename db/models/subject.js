const mongoose = require('mongoose');

const subjectSchema = new mongoose.Schema(
  {
    title: {
      required: true,
      type: String,
      unique: true
    }
  }
)
const subjectModel = mongoose.model('subject', subjectSchema);
module.exports = subjectModel;
