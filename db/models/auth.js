const mongoose = require('mongoose');
const authSchema = new mongoose.Schema({
  mobileNumber: {
    type: String,
    required: true
  },
  userId: {
    type: URLSearchParams
  }
});
const authModel = mongoose.model('auth',authSchema);
module.exports = authModel;

