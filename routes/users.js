var express = require('express');
var router = express.Router();
const UserModel = require('../db/models/user');
const jwt = require('jsonwebtoken');

/* GET users listing. */
router.get('/',async (req, res) => {
  const allUsers = await UserModel.find();
  res.send(allUsers);
});

router.post('/',async (req, res) => {
  const {body} = req;
  const createNewUser = await UserModel.create(body);
  res.send(createNewUser);
});

router.put('/update-profile', authM, async (req,res) => {
  
  const {body} = req;
  const {_id} =req.user;
  

  const updateUser = await UserModel.findByIdAndUpdate(_id , body);
  
    res.send({
      message: 'User updated successfully.',
      updateUser
    });
 
});

router.delete('/myself', authM, async (req, res) => {
  const { _id} = req.user;

  const deleteUser = await UserModel.findByIdAndDelete(_id);
  
  if (deleteUser) {
    res.send('user deleted successfully.');
  } else {
    res.send('user not found!');
  }
});

router.delete('/:id', authM, async (req, res) => {
  const {id} = req.params;
  const deleteUser = await UserModel.findByIdAndDelete(id);
  if(deleteUser){
    res.send('user deleted successfully.');
  }else{
    res.send('user not found!');
  }
})



function m(msg, res) {
  console.log(msg);
  res.send(msg);
}

function authM(req, res, next) {
  // check if token exists in header
  // Check if token is valid or not?
  // If token is valid then call next()
  // If not valid then send error response. (Unauthorised message)

  const {authorization} = req.headers;
  //console.log(authorization);
  const token = authorization.replace('Bearer ', '');
  
  if(token){
    const payload = jwt.verify(token, 'shhhhh');
    req.user = payload;
    next();
  }else{
    res.send('Unauthorised user');
  }

}

router.get('/fetch-something', 
  (req, res, next) => {
    console.log('1');
    req.user = { name: 'Snehlata'};
    // res.send('asdf')
    next();
  },
  (req, res, next) => { console.log(req.user);
    next();
  },
  (error, req, res, next) => { m(error, res) },
  (req, res) => {
    res.send('some')
  },
)

module.exports = router;
