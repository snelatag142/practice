const express = require('express');
const router = express.Router();
const ClassModel = require('../db/models/class');

router.get('/',async (req, res) => {
  const allClasses = await ClassModel.find();
  res.send(allClasses);
});

router.post('/', async (req, res) => {
  const {body} = req;
  const createdClass = await ClassModel.create(body);
  res.send(createdClass);
});

router.put('/:id', async (req, res) => {
  const {body,params: {id}} = req;
  console.log(id);
  // Find in db by Id,
  // Whether item exists in db or not.
  // If exists then update.
  // if not exists then throw entity not found error.

  const classDocument = await ClassModel.findById(id);
  console.log(classDocument);
  if(classDocument) {
    await classDocument.update(body);
    res.send('Class updated successfully');
  } else {
    res.send('Class not found!');
  }
});

router.get('/:id',async (req, res) => {
  const {params: {id}} = req;
 
  
  const getExistingClass = await ClassModel.findById(id);
  
  
  if(getExistingClass){
    res.send(getExistingClass);
  }else{
    res.send('Class not found!');
  }
 
});

router.delete('/:id',async (req, res) => {
  const {params: {id}} = req;
  const deleteClass = await ClassModel.findByIdAndDelete(id);
  if(deleteClass) {
    res.send('Class deleted successfully!');
  } else {
    res.send('Not found!');
  }
});



module.exports = router;