const express = require('express');
const router = express.Router();
const SubjectModel = require('../db/models/subject');

 //const subjects = [];
router.get('/', async (req,res) => {
  const allSubjects = await SubjectModel.find();
  res.send(allSubjects);
})

router.post('/',async (req,res) => {
  const {body} = req;
  const createdSubjects = await SubjectModel.create(body);
  res.send(createdSubjects);
})

router.get('/:id',async (req,res) => {
  const {params:{id}} = req;
  const getExistingSubject = await SubjectModel.findById(id);
  console.log(getExistingSubject);
  
  if(getExistingSubject){
    res.send(getExistingSubject);
  } else {
    res.send('Subject not found');
  }
  
})

router.put('/:id',async (req,res) => {
  const {body,params : {id}} = req;
  const updateExistingSubject = await SubjectModel.findById(id);
  if(updateExistingSubject){
    await updateExistingSubject.update(body);
    res.send('subject updated successfully');
  }else{
    res.send('subject not found');
  }
});

router.delete('/:id',async (req , res) => {
  const {params : {id}} = req;
  const deleteSubject = await SubjectModel.findById(id);
  if(deleteSubject){
    await deleteSubject.delete();
    res.send('deleted successfully');
  }else{
    res.send('subject not found');
  }
});

module.exports = router;