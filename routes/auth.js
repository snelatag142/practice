const express = require('express');
const userModel = require('../db/models/user');
const { generateOTP } = require('../utilities');
const router = express.Router();
const jwt = require('jsonwebtoken');

router.post('/request-otp', async (req,res) => {

  const {body: {mobileNumber}} = req;
  const otp = generateOTP(); // Utility function to generate otp.

  let user = await userModel.findOne({
    mobileNumber
  });

  if (!user) {
    user = await userModel.create({
      mobileNumber,
      otp
    });
  } else {
    await user.update({
      otp
    });
  }

  // Send otp to mobile number.
  // TODO: 
  // await smsService.sendOTP(user, otp);

  res.send('OTP sent to registered mobile number.');

  // Find user by mobile number
  // If user exists then
  // Generate otp and update user document.
  
  // If user don't exists then
  // Generate OTP and create a new document with given mobile number.

  // Send otp to mobile number
  // Send response that otp is sent to registered number.

});

router.post('/login', async (req,res) => {
  const {mobileNumber, otp}= req.body;
  const user = await userModel.findOne({mobileNumber});

  if(user){
    const x = user.otp;
    if(otp == x) {
      user.otp = '';
      user.save();
      const token = jwt.sign(user.toJSON(), 'shhhhh');
      

      res.send({
        message: 'logged in successfully.',
        token
      });
    } else {
      res.send('otp did not match!');
    }
  } else {
      res.send('you are not registered!');
  }
});

router.post('/create-admin',(req,res)=> {
  const {body} = req;
  res.send(body);
});

router.post('/login-admin',(req,res) => {
  const {body} = req;
  res.send(body);
});

module.exports = router;